import React from 'react';

import { View, Text } from 'react-native';

/* eslint-disable-next-line */
export interface HeaderProps {
  text: string;
}

export function Header(props: HeaderProps) {
  return (
    <View>
      <Text>{props.text}</Text>
    </View>
  );
}

export default Header;
