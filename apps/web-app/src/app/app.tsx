import React from 'react';

import NxWelcome from './nx-welcome';
import { Header } from '@multiplatform-app/shared/ui/layout';

export function App() {
  return (
    <>
      <Header text={'This is the shared/ui/layout library Header component'} />
      <NxWelcome title="web-app" />
    </>
  );
}

export default App;
